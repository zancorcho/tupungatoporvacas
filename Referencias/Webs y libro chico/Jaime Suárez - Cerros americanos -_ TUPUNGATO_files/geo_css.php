/* 348 13:37:15 */
body{ 
    margin:0px; 
    padding:0px; 
    font-family: 'Open Sans', sans-serif; 
    color:#555;
}
img{ vertical-align:middle }
#widgeonet_div_last_visits2 ul {
    list-style-type:none;
    margin:0;
    padding:3px;
}
#widgeonet_div_last_visits2{
        background:url("http://www.widgeo.net/geocompteur/img/tmp/geoipod_w.png");
	width:164px;
	height:314px;
}
.widgeonet_div_last_visits2_today {
	clear: both;
	width:162px;
        height:;
	font: normal 13px 'Open Sans', sans-serif;
	margin:70px 0;
	padding:5px 0 7px 0;
	color:#555;
	text-shadow: 1px 1px 1px #FFFFFF;}
#spywidgeonet {
	width:156px;
	height:306px;
	cursor:pointer;
}
.spyWrapper{ padding-top:2px }
li.widgeonet_div_last_visits2_item {
	clear:both;
	font: normal 13px 'Open Sans', sans-serif;
	color:#555;
	padding:3px 5px 5px 8px;
	border-bottom:1px solid #666;
	height:20px !important;
	max-height:20px !important;
}
div.widgeonet_div_last_visits2_subitem {
	background:#f9f9f9; padding:2px; height:41px; overflow: hidden; width:100%
}
#swf{
	z-index:0;
	position:absolute;
        opacity: 1;
        width:36px;
        height:36px;
        filter: alpha(opacity=100);
	top:217px;
	left:60px;
        background:transparent url("http://www.widgeo.net/geocompteur/img/tmp/earth_blue.png");
}
#lien_div{  
    margin:10px;
    text-align: center;
}
#lien_div a, .widgeonet_div_last_visits2_today a{
	font: normal 13px 'Open Sans', sans-serif;
	color: #555;
}
#lien_div a:hover, .widgeonet_div_last_visits2_today a:hover{
    text-decoration: none;
}
.tooltip {
  background:transparent url("http://www.widgeo.net/geocompteur/css/black_arrow.png");
  display:none; font:bold 12px 'Open Sans', sans-serif; height:65px; width:130px; padding:13px; color:#fff;
}
.monpays{
    text-align:center;
    overflow:hidden;
    font-size:16px;
    padding:7px 5px 0px 15px;

}
.effect3{
    box-shadow: 0 2px 8px rgba(0,0,0,0.4);
    -webkit-box-shadow: 0 2px 8px rgba(0,0,0,0.4);
}  
.blink {
    animation-duration: 1s;
    animation-name: blink;
    animation-iteration-count: infinite;
    animation-direction: alternate;
    animation-timing-function: ease-in-out; 
}
@keyframes blink {
    from {
        opacity: 1;
    }
    to {
        opacity: 0;                    
    }
}



/* START TOOLTIP STYLES */
[tooltip] {
  position: relative; /* opinion 1 */
}

/* Applies to all tooltips */
[tooltip]::before,
[tooltip]::after {
  text-transform: none; /* opinion 2 */
  font-size: 12px; /* opinion 3 */
  line-height: 1;
  user-select: none;
  pointer-events: none;
  position: absolute;
  display: none;
  opacity: 0;
  text-shadow: none;
}
[tooltip]::before {
  content: '';
  border: 5px solid transparent; /* opinion 4 */
  z-index: 1001; /* absurdity 1 */
}
[tooltip]::after {
  content: attr(tooltip); /* magic! */
  
  /* most of the rest of this is opinion */

  text-align: center;
  
  /* 
    Let the content set the size of the tooltips 
    but this will also keep them from being obnoxious
    */
  min-width: 3em;
  max-width: 21em;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  padding: 8px;
  border-radius: .3ch;
  box-shadow: 0 1em 2em -.5em rgba(0, 0, 0, 0.35);
  background: #333;
  color: #fff;
  z-index: 1000; /* absurdity 2 */
}

/* Make the tooltips respond to hover */
[tooltip]:hover::before,
[tooltip]:hover::after {
  display: block;
}

/* don't show empty tooltips */
[tooltip='']::before,
[tooltip='']::after {
  display: none !important;
}

/* FLOW: UP */
[tooltip]:not([flow])::before,
[tooltip][flow^="up"]::before {
  bottom: 100%;
  border-bottom-width: 0;
  border-top-color: #333;
}
[tooltip]:not([flow])::after,
[tooltip][flow^="up"]::after {
  bottom: calc(100% + 5px);
}
[tooltip]:not([flow])::before,
[tooltip]:not([flow])::after,
[tooltip][flow^="up"]::before,
[tooltip][flow^="up"]::after {
  left: 50%;
  transform: translate(-50%, -.5em);
}

/* FLOW: DOWN */
[tooltip][flow^="down"]::before {
  top: 100%;
  border-top-width: 0;
  border-bottom-color: #333;
}
[tooltip][flow^="down"]::after {
  top: calc(100% + 5px);
}
[tooltip][flow^="down"]::before,
[tooltip][flow^="down"]::after {
  left: 50%;
  transform: translate(-50%, .5em);
}

/* FLOW: LEFT */
[tooltip][flow^="left"]::before {
  top: 50%;
  border-right-width: 0;
  border-left-color: #333;
  left: calc(0em - 5px);
  transform: translate(-.5em, -50%);
}
[tooltip][flow^="left"]::after {
  top: 50%;
  right: calc(100% + 5px);
  transform: translate(-.5em, -50%);
}

/* FLOW: RIGHT */
[tooltip][flow^="right"]::before {
  top: 50%;
  border-left-width: 0;
  border-right-color: #333;
  right: calc(0em - 5px);
  transform: translate(.5em, -50%);
}
[tooltip][flow^="right"]::after {
  top: 50%;
  left: calc(100% + 5px);
  transform: translate(.5em, -50%);
}

/* KEYFRAMES */
@keyframes tooltips-vert {
  to {
    opacity: .9;
    transform: translate(-50%, 0);
  }
}

@keyframes tooltips-horz {
  to {
    opacity: .9;
    transform: translate(0, -50%);
  }
}

/* FX All The Things */ 
[tooltip]:not([flow]):hover::before,
[tooltip]:not([flow]):hover::after,
[tooltip][flow^="up"]:hover::before,
[tooltip][flow^="up"]:hover::after,
[tooltip][flow^="down"]:hover::before,
[tooltip][flow^="down"]:hover::after {
  animation: tooltips-vert 300ms ease-out forwards;
}

[tooltip][flow^="left"]:hover::before,
[tooltip][flow^="left"]:hover::after,
[tooltip][flow^="right"]:hover::before,
[tooltip][flow^="right"]:hover::after {
  animation: tooltips-horz 300ms ease-out forwards;
}